var url = "https://swapi.co/api/people/";
var favs = 0;

$(document).ready(function() {
  getData(url);
});

function addFav(id) {
  var el = $("#chk_" + id);

  if ($(el).prop("checked") === true) {
    if (favs >= 5) {
      alert("Cannot have more than 5 Favourite Characters!");
      $(el).prop("checked", false);
    } else {
      favs++;
      $(el)
        .siblings("span")
        .show();
    }
  } else {
    favs--;
    $(el)
      .siblings("span")
      .hide();
  }
}

function getData(url) {
  $("#loader").show();
  $("#nav").html("");
  $("#characters").html("");
  favs = 0;

  $.getJSON(url, function(res) {
    //console.log(res);
    var next = res.next;
    var prev = res.previous;
    var data = res.results;
    var tbody = "";

    $.each(data, function(i, obj) {
      var charUrl = obj.url;
      var arr = charUrl.split("/");
      //console.log(arr);
      var id = arr[arr.length - 2];

      tbody +=
        "<tr><td>" +
        obj.name +
        "</td><td>" +
        obj.height +
        "</td><td>" +
        obj.mass +
        "</td><td>" +
        obj.gender +
        '</td><td><a target="_blank" href="detail.html#' +
        id +
        '"><i class="fa fa-eye"></i> Details</a>' +
        '</td><td><input id="chk_' +
        id +
        '" type="checkbox" onclick="addFav(' +
        id +
        ')" />&nbsp;<span class="fav"><i class="fa fa-heart"></i></span></td>' +
        "</tr>";
    });

    $("#loader").hide();
    $("#characters").html(tbody);

    getNav(next, prev);
  });
}

function getNav(next, prev) {
  var nav = "";
  if (prev != "" && prev != null)
    nav += '<a href="' + prev + '" class="btn btn-primary prev">Prev</a>';

  if (next != "" && next != null)
    nav += '<a href="' + next + '" class="btn btn-primary pull-right">Next</a>';

  $("#nav").html(nav);

  $("#nav a").click(function() {
    getData($(this).attr("href"));
    return false;
  });
}
