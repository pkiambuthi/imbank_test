var charId = location.hash;
var url = "https://swapi.co/api/people/";

$(document).ready(function() {
  if (charId.length < 2) {
    alert("There is no character ID supplied!");
    $("#loader").hide();
    return false;
  }

  charId = charId.replace("#", "");
  url = url + charId;

  $.getJSON(url, function(res) {
    var tbody = "<tr><th>Name</th><td>" + res.name + "</td></tr>";
    tbody += "<tr><th>Gender</th><td>" + res.gender + "</td></tr>";
    tbody += "<tr><th>Birth Year</th><td>" + res.birth_year + "</td></tr>";
    tbody += "<tr><th>Height</th><td>" + res.height + "</td></tr>";
    tbody += "<tr><th>Mass</th><td>" + res.mass + "</td></tr>";
    tbody += "<tr><th>Eye Color</th><td>" + res.eye_color + "</td></tr>";
    tbody += "<tr><th>Skin Color</th><td>" + res.skin_color + "</td></tr>";
    tbody += "<tr><th>Hair Color</th><td>" + res.hair_color + "</td></tr>";

    $("#loader").hide();
    $("#details").append(tbody);
  });
});
