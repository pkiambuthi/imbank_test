﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeTest
{
    class Qstn2
    {

        //-- Constructor --//
        public Qstn2()
        {
            int[] arr1 = { 31, 26, 31, 33, 35, 42, 44, 31 };
            //int[] arr1 = { 1, 3, 2, 0 };

            Console.WriteLine("\nArray before sort\n---------------------------");
            foreach (int i in arr1)
                Console.WriteLine("Index \t{0}: ", i);

            sort(ref arr1);

            Console.WriteLine("\nArray after sort\n---------------------------");
            foreach (int i in arr1)
                Console.WriteLine("Index \t{0}: ", i);

            int val = 31;
            int ret = search(arr1, val);
            Console.WriteLine("\nSearch for value {0} : {1}", val, ret);

            Console.ReadKey();
        }
        

        //-- Performs a bubble sort on the array items --//
        private void sort(ref int[] arr)
        {
            int len = arr.Length;
            int tmp;

            for(int i=0; i<len-1; i++)
            {
                for(int j=0; j<len-1; j++)
                {
                    if(arr[j] > arr[j + 1])
                    {
                        tmp = arr[j + 1];
                        arr[j + 1] = arr[j];
                        arr[j] = tmp; 
                    }
                }
            }  
        }

        //-- Performs a binary search on the array items --//
        private int search(int[] arr, int val)
        {
            int ret = -1;
            int min = 0;
            int max = arr.Length - 1;
            
            while(min <= max)
            {
                int mid = (min + max) / 2;
                if (val == arr[mid])
                {
                    ret = mid;
                    max = mid - 1; //-- Check if the value exists on a lower index --//
                }
                else if (val < arr[mid])
                {
                    max = mid - 1;
                }
                else
                {
                    min = mid + 1;
                }
            }
            //-- if value is not found return -1 --//
            return ret;
        }


    }
}
