﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeTest
{
    public static class Qstn1
    {
        //-- Checks if a number is Prime --//
        static Func<int, bool> isPrime = num =>
        {
            if (num < 2) return false;
            if (num == 2) return true;

            int max = (int)Math.Floor(Math.Sqrt(num));

            for (int i = 2; i <= max; i++)
                if (num % i == 0) return false;

            return true;
        };

        //-- returns a memoized version of a function --//
        public static Func<T, TResult> Memoize<T, TResult>(this Func<T, TResult> func)
        {
            var cache = new ConcurrentDictionary<T, TResult>();
            return a => cache.GetOrAdd(a, func);
        }

        //-- run code --//
        public static void runCode()
        {
            var memoizedIsPrime = isPrime.Memoize();
            int num = 7;
            bool ret = memoizedIsPrime(num);

            Console.WriteLine("{0} isPrime?\t{1}", num, ret);
            Console.ReadKey();
        }

    }
}
